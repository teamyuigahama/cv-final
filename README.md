# Image Generation using Deep Conditional GANs
Source code for the final project of COMS 4731 Computer Vision Fall 2018.

## Preprequisite
- Python 3.6
- Pytorch 0.41
- cuDNN (GPU)

## Source code and READMEs
We provide our source code that we used to carry out all of the experiments in our final report. The source code is splitted into two directories - `mnist-cifar` and `imagenet`. Please refer to the READMEs in each individual directory for further details.

1. `mnist-cifar` contains the code for all experiments that we ran on the MNIST and the CIFAR10 dataset.

2. `imagenet` contains the data preprocessors, dataloader and the code that we use to evaluate our model on the ImageNet dataset.

## Contributors
- Steven Shao (ys2833)
- Andy Xu (lx2180)
