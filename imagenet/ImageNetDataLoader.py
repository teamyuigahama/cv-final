from torch.utils.data import Dataset
import pickle
from torchvision import transforms
from torch import from_numpy

OUTPUT_SAMPLES_PATH = "imagenet64/train_data/"
OUPPUT_SAMPLES_FORMAT = "{}/batch_{}_sample_{}.pkl"

class ImageNetDataset(Dataset):
    def __init__(self, cat2words2vec_pickle_path, batch_dataset_path, transform=None, use_subset = 1.0):
        with open(cat2words2vec_pickle_path, 'rb') as f:
            cat2words2vec_pickle = pickle.load(f, encoding='bytes')

        self.batch_sizes  = cat2words2vec_pickle['batch_sizes']
        self.batch_size = self.batch_sizes[0]
        for i in range(len(self.batch_sizes) - 1):
            assert(self.batch_sizes[i] == self.batch_size)
        self.transform=transform
        self.use_subset = use_subset
        print('batch_sizes:{}'.format(self.batch_sizes))
    def __len__(self):
        return int (sum(self.batch_sizes) * self.use_subset)
    def __getitem__(self, idx):
        batch_id = idx // self.batch_size
        if batch_id == 10:
            batch_id = 9
            sample_id = idx % self.batch_size + self.batch_size
        else:
            sample_id = idx % self.batch_size
        fname = OUTPUT_SAMPLES_PATH + OUPPUT_SAMPLES_FORMAT.format(batch_id, batch_id, sample_id)
        with open(fname, 'rb') as f:
            p = pickle.load(f)
        raw_image = p['image']
        raw_word_embedding = p['embeddings']
        raw_word = p['word']

        img_transform = transforms.Compose([transforms.ToTensor()])

        image = img_transform(raw_image)
        word_embedding = from_numpy(raw_word_embedding).float()

        return (word_embedding, image, raw_word)

