# Conditional GAN on ImageNet

## Data Preparation
1. Download the imagenet64x64 data from ImageNet (http://www.image-net.org/). Unzip all `train_data_batch_*` files to `imagenet64/`.
2. Download `glove.6B.100d.txt` and unzip the file to `imagenet64/`.
3. Make sure `map_clsloc.txt` is available under `imagenet64/`
4. Create another empty directory named `train_data` under `imagenet64`.
5. Run the `preprocess_imgnet_labels.ipynb` notebook to generate the preprocessed image-label training data set.
6. Make sure that the dictionary file `imagenet64/cat2words2vec.pkl` is created and the `imagenet64/train_data` is populated with 10 batches.
7. Create an empty directory `output/` to be filled with trained models and sample generated output.

## Training the network

To train the conditional GAN network, simply run this notebook `conditional-dcgan-imagenet-finer-vae-optional.ipynb`. It will read by default, from the genereated dictionary file and train on the preprocessed data from the previous step.

You can adjust the training parameter in the first cell,

```python
WORD_EMBEDDING_DIM = 100
CONDITION_DIM = 32 # the size of encoded word embedding vector
D_CONDITION_DIM = CONDITION_DIM
SEED_DIM = 64 # the size of the random seed (z)

NUM_KERNELS = 32
NUM_COLORS = 3

NUM_EPOCHS = 500
BATCH_SIZE = 128
USE_VAE = False
```

For each epoch, the model will save to `output/` and generate sample images on 15 labels to the same directory.

