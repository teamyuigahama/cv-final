from time import time
import torch
## utility functions for setting and getting durations of operations
## (in string or floating # seconds)
c_time = {None: 0}
## get duration since last stopwatch()
def get_stopwatch(key=None):
    global c_time
    if key not in c_time: c_time[key] = time()
    return time() - c_time[key]

## reset stopwatch with particular name `key` and return the duration so far
def stopwatch(key=None):
    x = get_stopwatch(key)
    global c_time
    c_time[key] = time()
    return x

def KL_loss(mu, logvar):
    # -0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD_element = mu.pow(2).add_(logvar.exp()).mul_(-1).add_(1).add_(logvar)
    KLD = torch.mean(KLD_element).mul_(-0.5)
    return KLD

def save_words(words, path):
    with open(path, 'w') as f:
        for i, word in enumerate(words):
            out = '{}, '.format(word)
            f.write(out)
