from time import time
## utility functions for setting and getting durations of operations
## (in string or floating # seconds)
c_time = {None: 0}
## get duration since last stopwatch()
def get_stopwatch(key=None):
    global c_time
    if key not in c_time: c_time[key] = time()
    return time() - c_time[key]

## reset stopwatch with particular name `key` and return the duration so far
def stopwatch(key=None):
    x = get_stopwatch(key)
    global c_time
    c_time[key] = time()
    return x