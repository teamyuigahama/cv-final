# MNIST and CIFAR models

Here we have the DCGANs and ACGANs that operate on MNIST and CIFAR. You should be able to run each notebook from the first cell down to the bottom without any errors, assuming everything is installed properly (PyTorch 0.4.1, along with some other scientific libraries like scipy, numpy, matplotlib). Each notebook loads (possibly downloads) the dataset, trains the model, and saves intermediate images and model checkpoints. Some notebooks have additional code after the training block that generates additional image figures, like nearest neighbours, or computing inception score.

A few notebooks are obsolete. Here are the descriptions of the notebooks that aren't (the ones mentioned in our report):

* `unconditional-fc-mnist.ipynb` - The unconditional fully-connected GAN on MNIST.
* `unconditional-fc-mnist.ipynb` - The fully-connected GAN on MNIST, conditioned on class labels.
* `conditional-dcgan-mnist.ipynb` - The conditional DCGAN on MNIST.
* `conditional-dcgan-cifar.ipynb'` - The MNIST conditional DCGAN applied to CIFAR. Includes inception score and compiling generated images from each class into an image file.
* `conditional-dcgan-cifar-finer-train.ipynb` - An improved version of the above with a deeper generator and discriminator, and smaller strides. Also include inception score and compiling generated images.
* `cifar-onehot-v3-train.ipynb` - An improved version of the above with larger kernels. Also include inception score and compiling generated images.
* `cifar-auxiliary-onehot-wider-train.ipynb` - The same as above, but using ACGAN for conditioning input instead of the regular conditioning formulation. Also include inception score and compiling generated images.
* `cifar-auxiliary-embed-wider-train.ipynb` - The same as above, but using embeddings as input. (Results were)