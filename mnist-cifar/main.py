import os
import torch
import torchvision
import torch.nn as nn
from torchvision import transforms
from torchvision.utils import save_image

device = torch.device('cuda' if torch.cuda.is_avialable() else 'cpu')

latent_size = 1
hidden_size = 1
image_size = 28 * 28
num_epochs = 200
batch_size = 100
sample_dir = 'samples'

if not os.path.exists(sample_dir): os.makedirs(sample_dir)

transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize(mean=(0.5,0.5,0.5), std=(0.5,0.5,0.5))

mnist = torchvision.datasets.MNIST(root='../mnist-data/', train=True, trnasform=transform, download=True)
